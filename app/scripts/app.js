'use strict';

/**
 * @ngdoc overview
 * @name simApp
 * @description
 * # simApp
 *
 * Main module of the application.
 */
angular
  .module('simApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .directive('onReadFile', function ($parse) {
  return {
    restrict: 'A',
    scope: false,
    link: function(scope, element, attrs) {
      var fn = $parse(attrs.onReadFile);

      element.on('change', function(onChangeEvent) {
        var reader = new FileReader();

        reader.onload = function(onLoadEvent) {
          scope.$apply(function() {
            fn(scope, {$fileContent:onLoadEvent.target.result});
          });
        };

        reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
      });
      }
    };
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
        .when('/cesar', {
            templateUrl: 'views/Cesar.html',
            controller: 'CesarCtrl'
        })
        .when('/replace', {
            templateUrl: 'views/replace.html',
            controller: 'ReplaceCtrl'
        })
        .when('/vizhenera', {
            templateUrl: 'views/vizhenera.html',
            controller: 'VizheneraCtrl'
        })
        .when('/perestanovka', {
            templateUrl: 'views/perestanovka.html',
            controller: 'PerestanovkaCtrl'
        })
        .when('/blocknote', {
            templateUrl: 'views/blocknote.html',
            controller: 'BlocknoteCtrl'
        })
        .when('/blocks', {
          templateUrl: 'views/blocks.html',
          controller: 'BlocksCtrl'
        })
        .when('/des', {
          templateUrl: 'views/des.html',
          controller: 'DesCtrl'
        })
        .when('/potok', {
          templateUrl: 'views/potok.html',
          controller: 'PotokCtrl'
        })
      .otherwise({
        redirectTo: '/'
      });

  });
