'use strict';

/**
 * @ngdoc function
 * @name simApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the simApp
 */
angular.module('simApp')
  .controller('MainCtrl', function ($scope) {
    $scope.strings = {
        str: "", // строка
        key: "",// ключ
        result: "",
        decode: ""
    }
    $scope.alphabet = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
        'a', 'A', 'b', 'B', 'c', 'C', 'd', 'D', 'e', 'E',
        'f', 'F', 'g', 'G', 'h', 'H', 'i', 'I', 'j', 'J',
        'k', 'K', 'l', 'L', 'm', 'M', 'n', 'N', 'o', 'O',
        'p', 'P', 'q', 'Q', 'r', 'R', 's', 'S', 't', 'T',
        'u', 'U', 'v', 'V', 'w', 'W', 'x', 'X', 'y', 'Y',
        'z', 'Z', ' '];
    $scope.encode = function(){
        $scope.key = $scope.strings.key.toString();
//        console.log($scope.key);
        while($scope.key.length < $scope.strings.str.length){
            $scope.key+=$scope.strings.key;
        }
        $scope.strings.result = "";
        for(var i = 0;i<$scope.strings.str.length;i++){
            var charStr = $scope.strings.str[i];
            var charKey = $scope.key[i];
//            console.log(char);
            var index = {
                str:_.indexOf($scope.alphabet, charStr),
                key:_.indexOf($scope.alphabet, charKey)
            }
            var symbol = $scope.alphabet[(index.str+index.key)%$scope.alphabet.length];
            $scope.strings.result += symbol;
        }
    }
    $scope.decode = function(){
        $scope.key = $scope.strings.key.toString();
//        console.log($scope.key);
        while($scope.key.length < $scope.strings.result.length){
            $scope.key+=$scope.strings.key;
        }
        $scope.strings.decode = "";
        for(var i = 0;i<$scope.strings.result.length;i++){
            var charStr = $scope.strings.result[i];
            var charKey = $scope.key[i];
//            console.log(char);
            var index = {
                str:_.indexOf($scope.alphabet, charStr),
                key:_.indexOf($scope.alphabet, charKey)
            }
            var indexOf = index.str-index.key;
            if(indexOf<0) indexOf+=$scope.alphabet.length;
            var symbol = $scope.alphabet[indexOf%$scope.alphabet.length];
            $scope.strings.decode += symbol;
        }
    }
  });
