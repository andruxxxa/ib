'use strict'

###*
# @ngdoc function
# @name simApp.controller:MainCtrl
# @description
# # MainCtrl
# Controller of the simApp
###

angular.module('simApp').controller 'BlocksCtrl', ($scope) ->

  $scope.strings =
    input: '123'
    encode:''
    key:''
    test:""

  t = 0x00



  Gost =

    sBlocks: [
      [0x000000, 0x000001, 0x000002, 0x000003],
      [0x000005, 0x000010, 0x000011, 0x000012],
      [0x000020, 0x000033, 0x000088, 0x000004],
      [0x000021, 0x000022, 0x000044, 0x000055],
      [0x000032, 0x000111, 0x000001,  0x000007],
      [0x000011, 0x000045, 0x000016, 0x000065],
      [0x000018, 0x000019, 0x000020,  0x000099],
      [0x000011, 0x000022, 0x00000034, 0x000043]
    ]

    generateHexString :  ->
      length = 58
      ret = ''
      while ret.length < length
        ret += Math.random().toString(16).substring(2)
      ret.substring 0, length

    str2bytes:(str) ->
      arr = []
      for i, char of str
        arr.push char.charCodeAt(0);
      arr
    subblocks:(arr, size) ->
      result = []
      temp = []
      for i, v of arr
        temp.push v
        if temp.length == size
          result.push temp
          temp = []
        else
          if parseInt(i) == (arr.length - 1)
            result.push temp
      result
    union:(arrays, size) ->
      temp = []
      result = []
      for arrId, array of arrays
        for i, arr of array
          for value in arr
            temp.push value
            if temp.length == size
              result.push temp
              temp = []
      if temp.length != 0
        result.push temp
      result

    shift:(arr, direction) ->
      if !direction?
        direction = "left"
      if direction = "left"
        for i, v of arr
          arr[i] = v << 11
      else
        for i, v of arr
          arr[i] = v >> 11
      arr
    bytes2str:(arr) ->
      str = ""
      for i, v of arr
        str+=String.fromCharCode v
      str
    encode:(str, key) ->
      arrBytes = @str2bytes(str)
      blocks = @subblocks(arrBytes, 32)
      oldBlocks = _.clone blocks
      newBlocks = []
      for blockId, block of blocks
        newBlock = []
        for i, value of block
          newBlock.push key[i]^value
        newBlocks.push newBlock
      blocks = newBlocks

      arrBlocks4bytes = []

      for block in blocks
        temp = []
        @subblocks(block, 4).forEach (block4byte) ->
          temp.push block4byte
        arrBlocks4bytes.push temp
#
      for arrIndex, arrBlock4Byte of arrBlocks4bytes
        for i, block of arrBlock4Byte
          for index, value of block
            block[index] = @sBlocks[i][index]^block[index]
      #console.log arrBlocks4bytes



      blocks = @union(arrBlocks4bytes, 32)

      for i, block of blocks
        blocks[i] = @shift(block)
      console.log oldBlocks, 'old'
      console.log blocks, 'blocks'
      for i, block of blocks
        console.log block
        for elIndex, el of block
          blocks[i][elIndex] = el^oldBlocks[i][elIndex]
      console.log blocks, 'new'

      string = ""

      for block in blocks
        string+=@bytes2str(block)


      string


  key = Gost.generateHexString()

  $scope.strings.key = key



  $scope.showContent = ($fileContent) ->
    console.log $fileContent
    $scope.strings.input = $fileContent

  $scope.crypt = ->
    result = Gost.encode($scope.strings.input, key)

    console.log result

    $scope.strings.encode = result




