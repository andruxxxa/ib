// Generated by CoffeeScript 1.9.2
(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name simApp.controller:MainCtrl
   * @description
   * # MainCtrl
   * Controller of the simApp
   */
  angular.module('simApp').controller('PotokCtrl', function($scope) {
    var Lrs, bytesToStr, encrypt, encryptByte, getBit, getBytes;
    $scope.strings = {
      key: '',
      str: '',
      encode: '',
      decode: '',
      check: ''
    };
    console.log('поточное шифрование...');
    Lrs = {
      '1': function(byte) {
        byte = ((((byte >> 0) ^ (byte >> 75) ^ (byte >> 81) ^ (byte >> 84)) & 1) << 85) | (byte >> 1);
        return byte;
      },
      '2': function(byte) {
        byte = ((((byte >> 0) ^ (byte >> 77) ^ (byte >> 83) ^ (byte >> 88)) & 1) << 90) | (byte >> 1);
        return byte;
      },
      '3': function(byte) {
        byte = ((((byte >> 0) ^ (byte >> 79) ^ (byte >> 83) ^ (byte >> 86)) & 1) << 88) | (byte >> 1);
        return byte;
      }
    };
    getBytes = function(str) {
      var bytes, i;
      bytes = [];
      i = 0;
      while (i < str.length) {
        bytes.push(str.charCodeAt(i));
        ++i;
      }
      return bytes;
    };
    bytesToStr = function(bytes) {
      var result;
      result = "";
      bytes.map(function(byte) {
        return result += String.fromCharCode(byte);
      });
      return result;
    };
    getBit = function(byte) {
      return Lrs[3](Lrs[2](Lrs[1](byte)));
    };
    encryptByte = function(byte, loopCount) {
      var bytes, i;
      if (loopCount == null) {
        loopCount = 10;
      }
      bytes = [];
      i = 0;
      while (i < loopCount) {
        byte = getBit(byte);
        i++;
      }
      return byte;
    };
    encrypt = function(message) {
      var bytes, i, newb, result;
      bytes = getBytes(message);
      console.log(bytes);
      newb = [];
      bytes.map(function(byte) {
        return newb.push(encryptByte(byte, 10));
      });
      i = 0;
      while (i < newb.length) {
        newb[i] = (newb[i] | newb[i + 2]) & newb[newb.length - (1 + i)];
        i++;
      }
      console.log(newb);
      result = bytesToStr(newb);
      return result;
    };
    return console.log(encrypt("rhugtughtuihguit", 10));
  });

}).call(this);

//# sourceMappingURL=potok.js.map
