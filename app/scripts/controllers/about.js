'use strict';

/**
 * @ngdoc function
 * @name simApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the simApp
 */
angular.module('simApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
